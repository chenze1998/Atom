#import <Foundation/Foundation.h>

@interface Plant : NSObject

@property (strong,nonatomic) NSString *type;

- (void)photosynthesis;
- (void)respiration;

@end

