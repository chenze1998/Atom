#import "flower.h"

@implementation flower

- (void)photosynthesis;
{
    NSLog(@"花朵 %@ 光合作用!", self.type);
}

- (void)respiration
{
    NSLog(@"花朵 %@ 呼吸作用!",self.type);
}

- (void)afforest:(NSString *)txt
{
    NSLog(@"flower %@ says: %@!", self.type, txt);
}

@end


