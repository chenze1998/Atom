#import "Animal.h"

@implementation Animal

- (void)eat
{
    NSLog(@"Animal eat!");
}

- (void)sleep
{
    NSLog(@"Animal sleep!");
}

@end
