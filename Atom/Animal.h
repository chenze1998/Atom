#import <Foundation/Foundation.h>

@interface Animal : NSObject

@property (strong,nonatomic) NSString *name;

- (void)eat;
- (void)sleep;

@end

