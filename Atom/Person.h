#import "Animal.h"

@interface Person : Animal

- (void)talk:(NSString *)text;

@end
