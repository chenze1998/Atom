#import "Tree.h"

@implementation Tree

- (void)photosynthesis;
{
    NSLog(@"大树 %@ 光合作用!", self.type);
}

- (void)respiration
{
    NSLog(@"大树 %@ 呼吸作用!",self.type);
}

- (void)afforest:(NSString *)txt
{
    NSLog(@"Tree %@ says: %@!", self.type, txt);
}

@end
