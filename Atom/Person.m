#import "Person.h"

@implementation Person

- (void)eat
{
    NSLog(@"Person %@ 吃!", self.name);
}

- (void)sleep
{
    NSLog(@"Person %@ 睡!",self.name);
}

- (void)talk:(NSString *)text
{
    NSLog(@"Person %@ says: %@!", self.name, text);
}

@end
