

#import "Plant.h"

@implementation Plant


- (void)photosynthesis
{
    NSLog(@"Plant photosynthesis!");
}

- (void)respiration
{
    NSLog(@"Plant respiration!");
}


@end
