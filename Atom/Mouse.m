#import "Mouse.h"

@implementation Mouse

- (void)eat
{
    NSLog(@"Mouse %@ 吃!", self.name);
}

- (void)sleep
{
    NSLog(@"Mouse %@ 睡了一天!",self.name);
}

- (void)makeRole
{
    NSLog(@"Mouse makeRole!");
}

@end
